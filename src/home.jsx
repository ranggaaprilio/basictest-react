import React, { Component } from 'react';
import List from './List';

class Apl extends Component{
    constructor(props){
      super(props)
      this.state={
        todoItem:'',
        items:[],
      }
    }
  
    /*---------------------------------------------------------------
    //API Standar Javascript
    componentDidMount(){
      fetch('https://jsonplaceholder.typicode.com/users')
        .then(response => response.json())
        .then(json => this.setState({api:json}))
    }
    ==============================================*/
  
    //Todo Items
    handleSubmit=(event)=>{
      event.preventDefault()
      this.setState({
        items :[...this.state.items,this.state.todoItem],
        todoItem:''
      })
  
    }
  
    handleChange=(event)=>{
      event.preventDefault()
      this.setState({
        todoItem:event.target.value
      })
      console.log(this.state.todoItem)
    }
  
    render(){
  
      // const {api}=this.state
  
      return(
       <div>
         <form onSubmit={this.handleSubmit}>
           <input type="text" value={this.state.todoItem} onChange={this.handleChange}/>
           <button>Add</button>
         </form>
        <List items={this.state.items} />
       </div>
      );
    }
  }

  export default Apl;