import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {createStore} from 'redux';
import {Provider} from 'react-redux';

const globalState={
    Togglestatus:false
}

//reducer

const rootReducer=(state=globalState,action)=>{
   if(action.type==='Change_Stat'){
       return{
           Togglestatus:!state.Togglestatus
       }
   }
    return state;
}

const storeRedux=createStore(rootReducer)

ReactDOM.render(<Provider store={storeRedux}><App /></Provider>, document.getElementById('root'));

//Rangga @sekolah coding 
//render element
// const name="Rangga Aprilio ";
// const element= <h1>Hallo {name}!!!</h1>;
// ReactDOM.render(element, document.getElementById('master')); 


//Rangga @sekolah coding
//Render function

// function greet(nama) {
//     return "hai "+nama
// }

// const elements=<h1>{greet(name)}</h1>
// ReactDOM.render(elements, document.getElementById('subs')); 

serviceWorker.unregister();
