const redux=require('redux');
const createStore=redux.createStore;

const IntialState={
    value:0,
    age:12
}

//reducer
const rootReducer=(state=IntialState,action)=>{
    // console.log(action)
    switch (action.type) {
        case 'ADD_AGE':
            return {
                ...state,
                age:state.age+1
            }
        case 'CHANGE_VALUE':
            return{
                ...state,
                value:state.value+action.newValue
            }
    
        default:
            return state
            break;
    }
}

//store

const store=createStore(rootReducer)
console.log(store.getState())

//Subscribe

store.subscribe(()=>{
    console.log('stored Change:',store.getState())
})

//Dispatch

store.dispatch({type:'ADD_AGE'})
store.dispatch({type:'CHANGE_VALUE',newValue:12})
console.log('40',store.getState())
