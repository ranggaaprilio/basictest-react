import React, { Component } from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
}from 'react-router-dom';

import App from './dasboard.jsx';
import Apl from './home.jsx';
import Api from './Api.jsx';
import Message from './message.jsx';




/*declare with function
function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
         <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}
End Declare */


class Basic extends Component{
 render(){
   return(
    <Router>
    <div>
      <ul>
        <li>
          <Link to="/dashboard">Dashboard</Link>
        </li>
        <li>
          <Link to="/Home">Home</Link>
        </li>
        <li>
          <Link to ="/api">API</Link>
        </li>
        <li>
          <Link to ="/message">Message</Link>
        </li>
      </ul>
      <hr/>

      <Switch>
        <Route exact path="/Home" >
            <Apl/>
        </Route>
        <Route path="/dashboard">
          <App/>
        </Route>
        <Route path="/api" >
          <Api/>
        </Route>
        <Route path="/message" >
          <Message/>
        </Route>
      </Switch>

    </div>
  </Router>

   )
      
 }
}

export default Basic;
