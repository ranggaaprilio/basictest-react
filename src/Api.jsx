import React, { Component } from 'react';
import axios from 'axios';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
}from 'react-router-dom';
// import Add from './Add.jsx';


class Api extends Component{
    constructor(props){
      super(props)
      this.state={
        api:[],
        formblog:{
          "id": 11,
          "name": "",
          "username": "Rangga",
          "email": "Chaim_McDermott@dana.io",
          "address": {
            "street": "Dayna Park",
            "suite": "Suite 449",
            "city": "Bartholomebury",
            "zipcode": "76495-3109",
            "geo": {
              "lat": "24.6463",
              "lng": "-168.8889"
            }
          },
          "phone": "(775)976-6794 x41206",
          "website": "conrad.com",
          "company": {
            "name": "Yost and Sons",
            "catchPhrase": "Switchable contextually-based project",
            "bs": "aggregate real-time technologies"
          }
        }
      }

    }

    getData=()=>{
      axios.get(` http://localhost:3001/users?_sort=id&_order=desc`)
      .then(res => {
        const persons = res.data;
        this.setState({api:persons });
      })
    }

    componentDidMount(){
     this.getData();
    }

    handleremove=(data)=>{
      console.log('data',data)
    if (window.confirm('Yakinkah kamu ingin menghapus nama ku?')) {
        axios.delete(`http://localhost:3001/users/${data}`)
             .then((res)=>{
              this.getData();
              alert('Tega Kamu sama aku!!')
             })
      }else{
        console.log('cancel')
      }

    }

    addprompt=()=>{
      let person = prompt("Please enter your name");
      if (person){
        let frmblognew={...this.state.formblog}
        let timestamp=new Date().getTime()
        frmblognew['id']=timestamp
        frmblognew['name']=person
        this.setState({
          formblog:frmblognew
        },()=>{
          this.postDataToAPI()
        })
      }else{
        console.log('cancel')
      }


    }

    postDataToAPI=()=>{
      axios.post('http://localhost:3001/users', this.state.formblog)
      .then((res)=>{
        alert('Success Submit Data!!')
        this.getData();
    })
    .catch(function (error) {
      alert('Error Submit Data Because',error)
      console.log(error);
    });
    }


    render(){
      const {api}=this.state
      return(
        <div>
      { /* <Router>
        <button><Link to="/add">Add</Link></button>
        <Switch>
          <Route exact path="/add" >
              <Add/>
          </Route>
        </Switch>
        </Router>*/}
        <button onClick={this.addprompt}>Add</button>
          <ul>
            {api.map((item,index)=>
              <li key={item.id} >{item.name} <button onClick={()=>this.handleremove(item.id)}>Remove</button></li>
            )}
        </ul>
        </div>
      )
    }

  }

  export default Api;
