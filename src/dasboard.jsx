
import React, { Component } from 'react';
import logo from './logo.svg';
import {connect} from 'react-redux'

function GetFull(gens){
    return <span>{gens.belakang}</span>
    }
    
    function Greeting(props) {
    return (
    <h1>Halo {props.name}- <GetFull belakang={props.full}/></h1>
    
     )
    }
    
    class Waktu extends Component{
        constructor(props){
          super(props)
          this.state={
            time:props.detik
          }
        }
    
        //Lifecycle
    
        componentDidMount(){
          //1000=1 detik
          this.TambahDetik=setInterval( ()=>this.tambah(),1000)
        }
    
        componentWillUnmount(){
          clearInterval(this.TambahDetik)
        }
    
        tambah=()=>{
          this.setState((state,props)=>({
              time:parseInt(state.time)+1
          }))
        }
    
        render(){
        return (
          <div>{this.state.time} Detik</div>
          )
        }
    
    }
    
    class Toggle extends Component{
      constructor(props){
        super(props)
        this.state={
          Togglestatus:true
    
         
        }
        this.Handclicker=this.Handclicker.bind(this)
      }
      Handclicker(){
        this.setState(state=>({
          Togglestatus:!state.Togglestatus
        }))
        alert(`Toggle Status is ${this.state.Togglestatus?'ON':'OFF'}`)
      }
    
      render(){
        console.log('hahah',this.props)
        return(
        <div><button onClick={this.props.ChangeStat}>{this.props.stat?'ON':'OFF'}</button></div>
        )
      }
    }
    
    class App extends Component{
      render(){
        console.log(this.props)
        return (
          <div className="App">
            <header className="App-header">
              <img src={logo} className="App-logo" alt="logo" />
              <p>
                Edit <code>src/App.js</code> and save to reload.
              </p>
              <p>
                <Greeting name="Aseanindo" full="Kapita Solusi"/>
              </p>
              <Waktu detik="0" />
              <Toggle stat={this.props.TogglestatusNew} ChangeStat={this.props.ChangeStat}/>
            </header>
          </div>
        );
      }
    }

    const mapStateToProps=(state)=>{
      return{
        TogglestatusNew:state.Togglestatus
      }
    }

    const mapDispatchToProps=(dispatch)=>{
      return{
        ChangeStat:()=>dispatch({type:'Change_Stat'})
      }
    }
    export default connect(mapStateToProps,mapDispatchToProps)(App);
    // export default App;