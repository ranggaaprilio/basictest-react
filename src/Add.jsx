import React, { Component } from 'react';
import axios from 'axios';
import './App.css';


class Add extends Component {

     state={
      formblog:{
        "id": 11,
        "name": "",
        "username": "Rangga",
        "email": "Chaim_McDermott@dana.io",
        "address": {
          "street": "Dayna Park",
          "suite": "Suite 449",
          "city": "Bartholomebury",
          "zipcode": "76495-3109",
          "geo": {
            "lat": "24.6463",
            "lng": "-168.8889"
          }
        },
        "phone": "(775)976-6794 x41206",
        "website": "conrad.com",
        "company": {
          "name": "Yost and Sons",
          "catchPhrase": "Switchable contextually-based project",
          "bs": "aggregate real-time technologies"
        }
      }
    }

  handleChange=(event)=>{
    let frmblognew={...this.state.formblog}
    let timestamp=new Date().getTime()
    frmblognew['id']=timestamp
    frmblognew[event.target.name]=event.target.value
    this.setState({
      formblog:frmblognew
    })

  }

  postDataToAPI=()=>{
    axios.post('http://localhost:3001/users', this.state.formblog)
    .then(function (response) {
      alert('Success Submit Data!!')
      console.log(response)
  })
  .catch(function (error) {
    alert('Error Submit Data Because',error)
    console.log(error);
  });
  }

  handleSubmit=()=>{
    this.postDataToAPI()
  }

  render(){
    return(
      <div>
      <br/>

      <input name="name" id="name" onChange={this.handleChange} placeholder="Masukan Nama Baru"/>
      <button onClick={this.handleSubmit}>Submit</button>


      </div>
    )
  }
}

export default Add;
